(require 'ironclad)

(defvar *chain* nil)

(defstruct block
  no ; which number in the chain of this block
  sender ; sender
  reserver ; reserver
  action ; what did they do
  previous-hash ; the whole chain's hash up until this block
  this-hash) ; store this block's data in a list (including previous-hash) and hash it here

(defun hashing (str &optional (hash-algo :sha256))
  "Hash the string str to the hashed hex string."
  (ironclad:byte-array-to-hex-string
   (ironclad:digest-sequence hash-algo
                             (ironclad:ascii-string-to-byte-array str))))

(defun hash-block (blck)
  "Hashing the block"
  (hashing (format nil "~s" blck)))

(defun create-block (sender reserver action)
  "Create the block and attach it to the chain."
  (let ((b (make-block))) ; the block itself
    (setf (block-no b) (if (zerop (length *chain*))
                           1
                           (1+ (length *chain*))))
    (setf (block-sender b) sender)
    (setf (block-reserver b) reserver)
    (setf (block-action b) action)
    (setf (block-previous-hash b)
          (if (zerop (length *chain*))
              0 ; previous hash of the first block is zero
              (hash-block (first *chain*)))) ; hash the lastest block on the chain or does it hash the whole chain before it?
    (setf (block-this-hash b) (hashing (format nil "~s"
                                               (list (block-no b)
                                                     (block-sender b)
                                                     (block-reserver b)
                                                     (block-action b)
                                                     (block-previous-hash b)))))
    (push b *chain*)))

(defun save-chain (&optional (filename "chain.db"))
  (with-open-file (out filename
                       :direction :output
                       :if-exists :supersede)
    (with-standard-io-syntax
      (format out "~s" *chain*))))

(defun load-chain (&optional (filename "chain.db"))
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *chain* (read in)))))

(defun check-block (blck)
  "check whethe the content of this block is valid or not"
  (let* ((this-block-number (block-no blck))
        (previous-block (nth (- (length *chain*) ; from all this amount of blocks
                                this-block-number) ; minus with this block's number and we'll have the position of this block on the chain
                             *chain*)))
    (if (string= (block-previous-hash blck)
                 (hash-block previous-block))
        t
        nil)))

(defmacro contract (&body body)
  "contract from <name> to <name> do <something>"
  `(progn
     (if (or (not (eql 'from ,(first body)))
             (not (eql 'to ,(third body)))
             (not (eql 'do ,(fifth body))))
         (error "Wrong command."))
     (create-block (quote ,(second body)) (quote ,(fourth body)) (quote ,(sixth body)))))
